package com.ims.common.core.asset;

/**
 * 
 * 类描述：<b>全局常量表</b> 
 * 创建人：陈骑元
 * 创建时间：2016-6-1 上午01:31:20
 * 修改人：蓝枫 
 * 修改时间：2016-6-1 上午01:31:20
 * 修改备注： 
 * @version
 */
public interface IMSCons {
	 
	public static final String SUPER_ADMIN="super";
	/**
	 * 当前登陆用户ID
	 */
	public static final String LOGIN_USER_ID="login_user_id";
	/**
	 * redis是否在线的键
	 * 
	 */
	public static final String REDIS_ISLIVE_KEY="redis.isLive";
    /**
     * 密码秘钥
     */
	public static final String PASSWORD_KEY = "IMSSYTEM";
	/**
	 * 界面风格 经典风格 1
	 */
	public static final String STYLE_CLASSIC="1";
	/**
	 * 界面风格  顶部布局
	 */
	public static final String STYLE_TOP_LAYOUT="2";
	/**
	 * 日期格式
	 */
	
	public static final String DATA = "yyyy-MM-dd";

	/**
	 * 日期时间格式
	 */
	public static final String DATATIME = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 存储过程业务成功状态码：成功
	 */
	public static final int PROC_SUCCESS = 1;

	/**
	 * 业务状态码：成功
	 */
	public static final int SUCCESS = 1;
	
	/**
	 * 业务状态吗：警告
	 */
	public static final int WARN = 0;

	/**
	 * 业务状态码：失败
	 */
	public static final int ERROR = -1;

	/**
	 * 字符布尔值：真
	 */
	public static final String STR_TRUE = "1";

	/**
	 * 字符布尔值：假
	 */
	public static final String STR_FALSE = "0";

	/**
	 * 系统运行模式：开发模式
	 */
	public static final String RUNAS_DEV = "0";

	/**
	 * 系统运行模式：生产模式
	 */
	public static final String RUNAS_PRO = "1";
	/**
	 * 是否标识
	 */
	public static final class IS {
		public static final String YES = "1";
		public static final String NO = "0";
	}

	/**
	 * Json输出模式。格式化输出模式。
	 */
	public static final String JSON_FORMAT = "0";

	/**
	 * Ext Reader对象的totalProperty属性名称
	 */
	public static final String READER_TOTAL_PROPERTY = "total";

	/**
	 * Ext Reader对象的root属性名称
	 */
	public static final String READER_ROOT_PROPERTY = "rows";

	/**
	 * Dto对象中的内部变量：交易状态码
	 */
	public static final String APPCODE_KEY = "appcode";

	/**
	 * Dto对象中的内部变量：交易状态信息
	 */
	public static final String APPMSG_KEY = "appmsg";
	
	/**
	 * 请求相应成功标志
	 */
	public static final String REQUEST_SUCCESS = "success";

	/**
	 * 控制台醒目标记1
	 */
	public static final String CONSOLE_FLAG1 = "● ";

	/**
	 * 控制台醒目标记2
	 */
	public static final String CONSOLE_FLAG2 = "●● ";
	
	/**
	 * 控制台醒目标记3
	 */
	public static final String CONSOLE_FLAG3 = "●●● ";

	/**
	 * UserInfo对象在Session中的key，Dto中的当前UserInfo也使用此Key
	 */
	public static final String USERINFOKEY = "sessionUserInfo";

	/**
	 * 获取前端UI选择模型选中的标识字段的数组，前端请求参数key应为：aos_rows_，方能取到。
	 */
	public static final String IMS_ROWS_ = "ims_rows_";
    
	
	/**
	 * 数序运算SQL的参数Dto中的运算表达式Key。
	 */
	public static final String CALCEXPR = "_expr";

	/**
	 * 系统皮肤：blue
	 */
	public static final String SKIN_BLUE = "blue";

	/**
	 * 系统皮肤：gray
	 */
	public static final String SKIN_GRAY = "gray";

	/**
	 * 系统皮肤：neptune
	 */
	public static final String SKIN_NEPTUNE = "neptune";

	/**
	 * 系统皮肤：aos
	 */
	public static final String SKIN_AOS = "aos";

	/**
	 * DTO缺省字符串Key
	 */
	public static final String DEFAULT_STRING_KEY = "_default_string_a";

	/**
	 * DTO缺省List Key
	 */
	public static final String DEFAULT_LIST_KEY = "_default_list_a";

	/**
	 * DTO缺省BigDecimal Key
	 */
	public static final String DEFAULT_BIGDECIMAL_KEY = "_default_bigdecimal_a";

	/**
	 * DTO缺省Integer Key
	 */
	public static final String DEFAULT_INTEGER_KEY = "_default_integer_a";

	/**
	 * DTO缺省Boolean Key
	 */
	public static final String DEFAULT_BOOLEAN_KEY = "_default_boolean_a";
	
	/**
	 * 会话中验证码的缺省Key
	 */
	public static final String VERCODE = "_vercode";
	
	/**
	 * WEBAPPCXT是否成功的标志KEY
	 */
	public static final String WEBAPPCXT_IS_SUCCESS_KEY = "_webappcxt_is_success";
	
	/**
	 * ContextPath在系统变量中的Key
	 */
	public static final String CXT_KEY = "cxt";
	
	/**
	 * JOSQL AOSListUtils 中使用的KEY
	 */
	public static final String IMSLIST_KEY = ":IMSList";
	
	/**
	 * 排序器在参数对象中的Key
	 */
	public static final String ORDER_KEY = "_order";
	
	/**
	 * 点击菜单节点自动拼接到URL上面的菜单模块编号参数
	 */
	public static final String MODULE_ID_KEY = "aos_module_id_";
	
	/**
	 * 点击主页面上二级导航页面的页面ID参数
	 */
	public static final String PAGE_ID_KEY = "aos_page_id_";
	
	/**
	 * 角色授权模式。1：可见当前管理员创建的角色和当前管理员所属组织的其他管理员创建的角色。
	 */
	public static final String ROLE_GRANT_MODE_NOCASCADE = "1";
	
	/**
	 * 角色授权模式。2：可见当前管理员创建的角色和当前管理员所属组织及其下级子孙组织的其他管理员创建的角色。
	 */
	public static final String ROLE_GRANT_MODE_CASCADE = "2";
	
	/**
	 * 快捷菜单布局风格。1：平铺。
	 */
	public static final String NAV_QUICK_LAYOUT_FLAT = "1";
	
	/**
	 * 快捷菜单布局风格。2：树状。
	 */
	public static final String NAV_QUICK_LAYOUT_TREE = "2";
	
	/**
	 * JDBC执行模式。1：只读模式。
	 */
	public static final String JDBC_EXECUTE_ONLYREAD = "1";
	
     /**
	 * 科目分类根ID
	 */
	public static final String TREE_ROOT_ID="0";
	/**
	 * 科目根节点名称
	 */
	public static final String TREE_ROOT_NAME="全部分类";

	/**
	 * 科目根节点语义ID
	 */
	public static final String TREE_ROOT_CASCADE_ID="0";
	/**
	 * 树的叶节点图标CSS类名
	 */
	public static final String TREE_LEAF_INCONCLS="tree_leaf";
	/**
	 * 树的节点打开
	 */
	public static final String TREE_STATE_OPEN="open";
	/**
	 * 树的节点关闭
	 */
	public static final String TREE_STATE_CLOSED="closed";
	/**
	 * 菜单的根节点的名称
	 */
	public static final String MENU_ROOT_NAME="功能菜单";
	/**
	 * 组织机构根节点名称
	 */
	public static final String DEPT_ROOT_NAME="组织机构";
	/**
	 * 菜单根节点图标
	 */
	public static final String MENU_ROOT_ICONCLS="book";
	/**
	 * 组织机构根节点图标
	 */
	public static final String DEPT_ROOT_ICONCLS="dept_config";
	/**
	 * JS头<br>
	 */
	public static final String SCRIPT_START = "<script type=\"text/javascript\">\n";
	
	/**
	 * JS尾<br>
	 */
	public static final String SCRIPT_END = "\n</script>";
	
	/**
	 * 对象删除规则
	 *
	 */
	public static  final class OBJECT_DELETE_RULE{
		//逻辑删除
		public static final String UPDATE = "update";
		//物理删除
		public static final String DELETE = "delete";
	}

	//缓存容器
	public static  final class CACHE{
		//字典、参数缓存
		public static final String IMSRESOURCECACHE = "imsResourceCache";
		//会话缓存
		public static final String IMSSESSIONCACHE = "imsSessionCache";
	}
	/**
	 * Cache对象前缀
	 *
	 */
	public static  final class CACHE_PREFIX{
		//全局参数
		public static final String PARAM = "ims.cache.param.key_:";
		//字典
		public static final String DIC = "ims.cache.dic.key_:";
		
	}

	
}
