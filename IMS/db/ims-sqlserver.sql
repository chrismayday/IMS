USE [ims]
GO
/****** Object:  Table [dbo].[SYS_CATALOG]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_CATALOG](
	[CATALOG_ID] [nvarchar](50) NOT NULL,
	[CASCADE_ID] [nvarchar](500) NULL,
	[ROOT_KEY] [nvarchar](100) NULL,
	[ROOT_NAME] [nvarchar](100) NULL,
	[CATALOG_NAME] [nvarchar](50) NULL,
	[PARENT_ID] [nvarchar](50) NULL,
	[SORT_NO] [int] NULL,
	[ICON_NAME] [nvarchar](50) NULL,
	[IS_AUTO_EXPAND] [nvarchar](10) NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_CATALOG] PRIMARY KEY CLUSTERED 
(
	[CATALOG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_DEPT]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_DEPT](
	[DEPT_ID] [nvarchar](50) NOT NULL,
	[CASCADE_ID] [nvarchar](255) NOT NULL,
	[DEPT_NAME] [nvarchar](100) NOT NULL,
	[PARENT_ID] [nvarchar](50) NULL,
	[DEPT_CODE] [nvarchar](50) NULL,
	[MANAGER] [nvarchar](50) NULL,
	[PHONE] [nvarchar](50) NULL,
	[FAX] [nvarchar](50) NULL,
	[ADDRESS] [nvarchar](200) NULL,
	[IS_AUTO_EXPAND] [nvarchar](10) NULL,
	[ICON_NAME] [nvarchar](50) NULL,
	[SORT_NO] [int] NULL,
	[REMARK] [nvarchar](400) NULL,
	[IS_DEL] [nvarchar](10) NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_DEPT] PRIMARY KEY CLUSTERED 
(
	[DEPT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_DICTIONARY]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_DICTIONARY](
	[DIC_ID] [nvarchar](50) NOT NULL,
	[DIC_INDEX_ID] [nvarchar](255) NULL,
	[DIC_CODE] [nvarchar](100) NULL,
	[DIC_VALUE] [nvarchar](100) NULL,
	[SHOW_COLOR] [nvarchar](50) NULL,
	[STATUS] [nvarchar](10) NULL,
	[EDIT_MODE] [nvarchar](10) NULL,
	[SORT_NO] [int] NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_DICTIONARY] PRIMARY KEY CLUSTERED 
(
	[DIC_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_DICTIONARY_INDEX]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_DICTIONARY_INDEX](
	[DIC_INDEX_ID] [nvarchar](50) NOT NULL,
	[DIC_KEY] [nvarchar](50) NULL,
	[DIC_NAME] [nvarchar](100) NULL,
	[CATALOG_ID] [nvarchar](50) NULL,
	[CATALOG_CASCADE_ID] [nvarchar](500) NULL,
	[DIC_REMARK] [nvarchar](500) NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_DICTIONARY_INDEX] PRIMARY KEY CLUSTERED 
(
	[DIC_INDEX_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_MENU]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_MENU](
	[MENU_ID] [nvarchar](50) NOT NULL,
	[CASCADE_ID] [nvarchar](500) NULL,
	[MENU_NAME] [nvarchar](100) NULL,
	[PARENT_ID] [nvarchar](50) NULL,
	[ICON_NAME] [nvarchar](50) NULL,
	[IS_AUTO_EXPAND] [nvarchar](10) NULL,
	[URL] [nvarchar](100) NULL,
	[REMARK] [nvarchar](500) NULL,
	[STATUS] [nvarchar](10) NULL,
	[EDIT_MODE] [nvarchar](10) NULL,
	[SORT_NO] [int] NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_MENU] PRIMARY KEY CLUSTERED 
(
	[MENU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_PARAM]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_PARAM](
	[PARAM_ID] [nvarchar](50) NOT NULL,
	[PARAM_NAME] [nvarchar](100) NULL,
	[PARAM_KEY] [nvarchar](50) NULL,
	[PARAM_VALUE] [nvarchar](500) NULL,
	[CATALOG_ID] [nvarchar](50) NULL,
	[CATALOG_CASCADE_ID] [nvarchar](500) NULL,
	[PARAM_REMARK] [nvarchar](200) NULL,
	[STATUS] [nvarchar](10) NULL,
	[EDIT_MODE] [nvarchar](10) NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nchar](10) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_PARAM] PRIMARY KEY CLUSTERED 
(
	[PARAM_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ROLE]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ROLE](
	[ROLE_ID] [nvarchar](50) NOT NULL,
	[ROLE_NAME] [nvarchar](100) NOT NULL,
	[STATUS] [nvarchar](10) NULL,
	[ROLE_TYPE] [nvarchar](10) NULL,
	[ROLE_REMARK] [nvarchar](400) NULL,
	[EDIT_MODE] [nvarchar](50) NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[CREATE_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
 CONSTRAINT [PK_SYS_ROLE] PRIMARY KEY CLUSTERED 
(
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ROLE_MENU]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ROLE_MENU](
	[ROLE_ID] [nvarchar](50) NOT NULL,
	[MENU_ID] [nvarchar](50) NOT NULL,
	[GRANT_TYPE] [nvarchar](10) NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[CREATE_TIME] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ROLE_USER]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ROLE_USER](
	[ROLE_ID] [nvarchar](50) NOT NULL,
	[USER_ID] [nvarchar](50) NOT NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[CREATE_TIME] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_USER]    Script Date: 2017/2/9 13:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_USER](
	[USER_ID] [nvarchar](50) NOT NULL,
	[ACCOUNT] [nvarchar](50) NULL,
	[PASSWORD] [nvarchar](50) NULL,
	[USERNAME] [nvarchar](50) NULL,
	[LOCK_NUM] [int] NULL,
	[ERROR_NUM] [int] NULL,
	[SEX] [nvarchar](10) NULL,
	[STATUS] [nvarchar](10) NULL,
	[USER_TYPE] [nvarchar](10) NULL,
	[DEPT_ID] [nvarchar](50) NULL,
	[MOBILE] [nvarchar](50) NULL,
	[QQ] [nvarchar](50) NULL,
	[WECHAT] [nvarchar](50) NULL,
	[EMAIL] [nvarchar](50) NULL,
	[IDNO] [nvarchar](50) NULL,
	[STYLE] [nvarchar](10) NULL,
	[ADDRESS] [nvarchar](200) NULL,
	[REMARK] [nvarchar](400) NULL,
	[IS_DEL] [nvarchar](10) NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_USER_ID] [nvarchar](50) NULL,
	[MODIFY_TIME] [datetime] NULL,
	[MODIFY_USER_ID] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_USER] PRIMARY KEY CLUSTERED 
(
	[USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[SYS_CATALOG] ([CATALOG_ID], [CASCADE_ID], [ROOT_KEY], [ROOT_NAME], [CATALOG_NAME], [PARENT_ID], [SORT_NO], [ICON_NAME], [IS_AUTO_EXPAND], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'0e6cca42523b4f95afb8d138dc533e61', N'0.002', N'DIC_TYPE', N'字典分类科目', N'数据字典分类', N'0', 1, N'book', N'0', CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_CATALOG] ([CATALOG_ID], [CASCADE_ID], [ROOT_KEY], [ROOT_NAME], [CATALOG_NAME], [PARENT_ID], [SORT_NO], [ICON_NAME], [IS_AUTO_EXPAND], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'4f39839093744dccaedc9d7dcdee4ab3', N'0.001', N'PARAM_TYPE', N'参数分类', N'参数分类科目', N'0', 1, N'book', N'0', CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_CATALOG] ([CATALOG_ID], [CASCADE_ID], [ROOT_KEY], [ROOT_NAME], [CATALOG_NAME], [PARENT_ID], [SORT_NO], [ICON_NAME], [IS_AUTO_EXPAND], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'5423b9ba9ac6472c80881827acafe9e9', N'0.001.001', N'PARAM_TYPE', N'参数分类', N'系统参数', N'4f39839093744dccaedc9d7dcdee4ab3', 1, N'folder', N'1', CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_CATALOG] ([CATALOG_ID], [CASCADE_ID], [ROOT_KEY], [ROOT_NAME], [CATALOG_NAME], [PARENT_ID], [SORT_NO], [ICON_NAME], [IS_AUTO_EXPAND], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'6a75051434b840a597aeb549e1e47ced', N'0.002.001', N'DIC_TYPE', N'字典分类科目', N'系统管理', N'0e6cca42523b4f95afb8d138dc533e61', 2, N'folder', N'1', CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_CATALOG] ([CATALOG_ID], [CASCADE_ID], [ROOT_KEY], [ROOT_NAME], [CATALOG_NAME], [PARENT_ID], [SORT_NO], [ICON_NAME], [IS_AUTO_EXPAND], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'b420860910f54c1d8901f099a9457f52', N'0.002.002', N'DIC_TYPE', N'字典分类科目', N'全局通用', N'0e6cca42523b4f95afb8d138dc533e61', 1, N'global', N'1', CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_DEPT] ([DEPT_ID], [CASCADE_ID], [DEPT_NAME], [PARENT_ID], [DEPT_CODE], [MANAGER], [PHONE], [FAX], [ADDRESS], [IS_AUTO_EXPAND], [ICON_NAME], [SORT_NO], [REMARK], [IS_DEL], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'0', N'0', N'组织机构', N'-1', NULL, NULL, NULL, NULL, NULL, N'1', N'dept_config', 1, N'顶级机构不能进行移动和删除操作，只能进行修改', N'0', CAST(0x0000A71500B9292C AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_DEPT] ([DEPT_ID], [CASCADE_ID], [DEPT_NAME], [PARENT_ID], [DEPT_CODE], [MANAGER], [PHONE], [FAX], [ADDRESS], [IS_AUTO_EXPAND], [ICON_NAME], [SORT_NO], [REMARK], [IS_DEL], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'08d8f82724864d2492df7c2f2a9b1294', N'0.0001', N'12', N'0', N'', N'', N'', N'', N'', N'1', N'', 1, N'', N'1', CAST(0x0000A71500B93034 AS DateTime), N'cb33c25f5c664058a111a9b876152317', CAST(0x0000A71500BB92C0 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'11b823f3b2e14e76bf94347a4a5e578e', N'c48507ef391d4e3d8d9b7720efe4841b', N'0', N'停用', NULL, N'1', N'0', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'293adbde400f457a8d947ff5c6341b04', N'992a7d6dbe7f4009b30cbae97c3b64a9', N'3', N'锁定', N'#FFA500', N'1', N'1', 3, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'2ac97527c4924127b742dd953d8b53ba', N'820d2a68425b4d8d9b423b81d6a0eec1', N'3', N'未知', NULL, N'1', N'1', 3, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'2bfc90a6917545cd87d73fb491292e2b', N'aaec0092a25b485f90c20898e9d6765d', N'1', N'缺省', NULL, N'1', N'1', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'3cf6af08f48e4cec913d09f67a0b3b43', N'992a7d6dbe7f4009b30cbae97c3b64a9', N'1', N'正常', NULL, N'1', N'1', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'82afb0bda8944af3a0e5f82608294670', N'0bf2a3cd7ed44516a261347d47995411', N'2', N'顶部布局', NULL, N'1', N'1', 2, CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317', CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'913ca1b4b49a434fb9591f6df0a52af8', N'c6f8b99b95c844b89dc86c143e04a294', N'0', N'否', NULL, N'1', N'0', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'9c63657b98c444e3bfd8a0a75128de2b', N'7a7faf68a5ec4f3cb9f45d89c119b26b', N'0', N'只读', NULL, N'1', N'0', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'a96dfb72b7b54e1989569a2b3c5f90ac', N'c48507ef391d4e3d8d9b7720efe4841b', N'1', N'启用', NULL, N'1', N'0', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'ca40ef37acef49f8930fcf22356ba50e', N'c6f8b99b95c844b89dc86c143e04a294', N'1', N'是', NULL, N'1', N'0', 2, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'd2cf230ce49040e3bf6e61a972659c09', N'992a7d6dbe7f4009b30cbae97c3b64a9', N'2', N'停用', N'red', N'1', N'1', 2, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'd404e540aab945df84a26e3d30b2dd47', N'820d2a68425b4d8d9b423b81d6a0eec1', N'2', N'女', NULL, N'1', N'1', 2, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'd7f0c4a5480d4dc4b3e6e4c5b405d9cb', N'820d2a68425b4d8d9b423b81d6a0eec1', N'1', N'男', NULL, N'1', N'1', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'e0e59a52f42c4263aac3e9dbbdb496df', N'0bf2a3cd7ed44516a261347d47995411', N'1', N'经典风格', NULL, N'1', N'1', 1, CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317', CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_DICTIONARY] ([DIC_ID], [DIC_INDEX_ID], [DIC_CODE], [DIC_VALUE], [SHOW_COLOR], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'f1c0ae8844504f96836b904ce81ac1bc', N'7a7faf68a5ec4f3cb9f45d89c119b26b', N'1', N'可编辑', NULL, N'1', N'0', 2, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'0bf2a3cd7ed44516a261347d47995411', N'layout_style', N'界面布局', N'6a75051434b840a597aeb549e1e47ced', N'0.002.001', NULL, CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317', CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'7a7faf68a5ec4f3cb9f45d89c119b26b', N'edit_mode', N'编辑模式', N'b420860910f54c1d8901f099a9457f52', N'0.002.002', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'820d2a68425b4d8d9b423b81d6a0eec1', N'sex', N'性别', N'6a75051434b840a597aeb549e1e47ced', N'0.002.001', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'992a7d6dbe7f4009b30cbae97c3b64a9', N'user_status', N'用户状态', N'6a75051434b840a597aeb549e1e47ced', N'0.002.001', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'aaec0092a25b485f90c20898e9d6765d', N'role_type', N'角色类型', N'6a75051434b840a597aeb549e1e47ced', N'0.002.001', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'c48507ef391d4e3d8d9b7720efe4841b', N'status', N'当前状态', N'b420860910f54c1d8901f099a9457f52', N'0.002.002', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_DICTIONARY_INDEX] ([DIC_INDEX_ID], [DIC_KEY], [DIC_NAME], [CATALOG_ID], [CATALOG_CASCADE_ID], [DIC_REMARK], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'c6f8b99b95c844b89dc86c143e04a294', N'is_auto_expand', N'是否自动展开', N'b420860910f54c1d8901f099a9457f52', N'0.002.002', NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'1ae9eeb251a243abb4c0c4f3865d6262', N'0.001.001', N'菜单配置', N'c66886c9ee47415aa81a6589acdb480a', N'menu_config', N'1', N'system/menu/init.jhtml', NULL, N'1', N'1', 4, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'8eefe6e3298c4203b21e85e354b284ab', N'0.001.004', N'分类科目', N'c66886c9ee47415aa81a6589acdb480a', N'catalog', N'1', N'system/catalog/init.jhtml', NULL, N'1', N'1', 7, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'94ca69dd59b84054ad056b130d959425', N'0.001.003', N'键值参数', N'c66886c9ee47415aa81a6589acdb480a', N'param_config', N'1', N'system/param/init.jhtml', NULL, N'1', N'1', 6, CAST(0x0000A69801888CF8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'a5b39c90931b4249a23e30f24303dbfa', N'0.001.002', N'数据字典', N'c66886c9ee47415aa81a6589acdb480a', N'dictionary', N'1', N'system/dictionary/init.jhtml', NULL, N'1', N'0', 5, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'c66886c9ee47415aa81a6589acdb480a', N'0.001', N'系统管理', N'0', N'system_manage', N'1', NULL, NULL, N'1', N'1', 10, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'd525b1f3274244c9af3a06c4e72621d8', N'0.001.009', N'角色管理', N'c66886c9ee47415aa81a6589acdb480a', N'group_link', N'1', N'system/role/init.jhtml', NULL, N'1', N'1', 3, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'dfc4df3c7f4341f885caf7aa305f8995', N'0.001.007', N' 组织机构', N'c66886c9ee47415aa81a6589acdb480a', N'dept_config', N'1', N'system/dept/init.jhtml', NULL, N'1', N'1', 1, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_MENU] ([MENU_ID], [CASCADE_ID], [MENU_NAME], [PARENT_ID], [ICON_NAME], [IS_AUTO_EXPAND], [URL], [REMARK], [STATUS], [EDIT_MODE], [SORT_NO], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'f278c724ce8e4649bc2b74333ac0d28c', N'0.001.008', N'用户管理', N'c66886c9ee47415aa81a6589acdb480a', N'user_config', N'1', N'system/user/init.jhtml', NULL, N'1', N'1', 2, CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A69801888CF8 AS DateTime), NULL)
INSERT [dbo].[SYS_USER] ([USER_ID], [ACCOUNT], [PASSWORD], [USERNAME], [LOCK_NUM], [ERROR_NUM], [SEX], [STATUS], [USER_TYPE], [DEPT_ID], [MOBILE], [QQ], [WECHAT], [EMAIL], [IDNO], [STYLE], [ADDRESS], [REMARK], [IS_DEL], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'12e9c8eaf2a041c999a520bda6ff3a4b', N'abc', N'4UZ05NepF7k=', N'1', 5, 0, N'3', N'1', N'1', N'0', N'', N'', N'', N'', N'', N'1', N'', N'1', N'1', CAST(0x0000A71500B94FD8 AS DateTime), N'cb33c25f5c664058a111a9b876152317', CAST(0x0000A71500BB8CE4 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
INSERT [dbo].[SYS_USER] ([USER_ID], [ACCOUNT], [PASSWORD], [USERNAME], [LOCK_NUM], [ERROR_NUM], [SEX], [STATUS], [USER_TYPE], [DEPT_ID], [MOBILE], [QQ], [WECHAT], [EMAIL], [IDNO], [STYLE], [ADDRESS], [REMARK], [IS_DEL], [CREATE_TIME], [CREATE_USER_ID], [MODIFY_TIME], [MODIFY_USER_ID]) VALUES (N'cb33c25f5c664058a111a9b876152317', N'super', N'0d+ywCe6ffI=', N'超级用户', 10, 0, N'2', N'1', N'2', N'0', N'13802907704', N'240823329', N'', N'240823329@qq.com', N'', N'1', N'', N'超级用户，拥有最高的权限', N'0', CAST(0x0000A69801888CF8 AS DateTime), NULL, CAST(0x0000A71500BE6248 AS DateTime), N'cb33c25f5c664058a111a9b876152317')
ALTER TABLE [dbo].[SYS_DEPT] ADD  CONSTRAINT [DF_SYS_DEPT_IS_DEL]  DEFAULT ((0)) FOR [IS_DEL]
GO
ALTER TABLE [dbo].[SYS_DICTIONARY] ADD  CONSTRAINT [DF_SYS_DICTIONARY_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO
ALTER TABLE [dbo].[SYS_DICTIONARY] ADD  CONSTRAINT [DF_SYS_DICTIONARY_EDIT_MODE]  DEFAULT ((1)) FOR [EDIT_MODE]
GO
ALTER TABLE [dbo].[SYS_MENU] ADD  CONSTRAINT [DF_SYS_MENU_IS_AUTO_EXPAND]  DEFAULT ((0)) FOR [IS_AUTO_EXPAND]
GO
ALTER TABLE [dbo].[SYS_MENU] ADD  CONSTRAINT [DF_SYS_MENU_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO
ALTER TABLE [dbo].[SYS_MENU] ADD  CONSTRAINT [DF_SYS_MENU_EDIT_MODE]  DEFAULT ((1)) FOR [EDIT_MODE]
GO
ALTER TABLE [dbo].[SYS_PARAM] ADD  CONSTRAINT [DF_SYS_PARAM_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO
ALTER TABLE [dbo].[SYS_PARAM] ADD  CONSTRAINT [DF_SYS_PARAM_EDIT_MODE]  DEFAULT ((1)) FOR [EDIT_MODE]
GO
ALTER TABLE [dbo].[SYS_ROLE] ADD  CONSTRAINT [DF_SYS_ROLE_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO
ALTER TABLE [dbo].[SYS_ROLE] ADD  CONSTRAINT [DF_SYS_ROLE_EDIT_MODE]  DEFAULT ((1)) FOR [EDIT_MODE]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_LOCK_NUM]  DEFAULT ((5)) FOR [LOCK_NUM]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_ERROR_NUM]  DEFAULT ((0)) FOR [ERROR_NUM]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_SEX]  DEFAULT ((3)) FOR [SEX]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_STYLE]  DEFAULT ((1)) FOR [STYLE]
GO
ALTER TABLE [dbo].[SYS_USER] ADD  CONSTRAINT [DF_SYS_USER_IS_DEL]  DEFAULT ((0)) FOR [IS_DEL]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类科目编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'CATALOG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类科目语义ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'CASCADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目标识键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'ROOT_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'科目名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'ROOT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'CATALOG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父节点编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'PARENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'SORT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'ICON_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动展开' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'IS_AUTO_EXPAND'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CATALOG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'节点语义ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'CASCADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组织名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'DEPT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父节点流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'PARENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'DEPT_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主要负责人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'MANAGER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部门电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'PHONE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'传真' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'ADDRESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动展开' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'IS_AUTO_EXPAND'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'节点图标文件名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'ICON_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'SORT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除 0有效 1删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'IS_DEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组织机构' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DEPT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'DIC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属字典流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'DIC_INDEX_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典对照码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'DIC_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典对照值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'DIC_VALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示颜色' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'SHOW_COLOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前状态(0:停用;1:启用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编辑模式(0:只读;1:可编辑)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'EDIT_MODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'SORT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据字典' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'DIC_INDEX_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'DIC_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'DIC_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属分类流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'CATALOG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属分类流节点语义ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'CATALOG_CASCADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'DIC_REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据字单类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_DICTIONARY_INDEX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'MENU_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类科目语义ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'CASCADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'MENU_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单父级编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'PARENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'ICON_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否自动展开' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'IS_AUTO_EXPAND'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'url地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前状态(0:停用;1:启用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编辑模式(0:只读;1:可编辑)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'EDIT_MODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'SORT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单配置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_MENU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'PARAM_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'PARAM_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数键名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'PARAM_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数键值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'PARAM_VALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目录ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'CATALOG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类科目语义ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'CATALOG_CASCADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'PARAM_REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前状态(0:停用;1:启用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编辑模式(0:只读;1:可编辑)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'EDIT_MODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'键值参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_PARAM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'ROLE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'ROLE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前状态 1启用 0禁用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'ROLE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'ROLE_REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编辑模式(0:只读;1:可编辑)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'EDIT_MODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色管理' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU', @level2type=N'COLUMN',@level2name=N'ROLE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU', @level2type=N'COLUMN',@level2name=N'MENU_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'权限类型 1 经办权限 2管理权限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU', @level2type=N'COLUMN',@level2name=N'GRANT_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色与菜单关联表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_MENU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_USER', @level2type=N'COLUMN',@level2name=N'ROLE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_USER', @level2type=N'COLUMN',@level2name=N'USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_USER', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_USER', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色与用户关联表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ROLE_USER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户登录帐号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'ACCOUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'USERNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'锁定次数 默认5次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'LOCK_NUM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码错误次数  如果等于锁定次数就自动锁定用户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'ERROR_NUM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别  1:男2:女3:未知' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'SEX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户状态 1:正常2:停用 3:锁定' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'USER_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属部门流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'MOBILE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QQ号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'QQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微信' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'WECHAT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'身份证号码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'IDNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'界面风格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'STYLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'ADDRESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'REMARK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除 0有效 1删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'IS_DEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'CREATE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'CREATE_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'MODIFY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER', @level2type=N'COLUMN',@level2name=N'MODIFY_USER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户管理' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_USER'
GO
